# Python Tests for automatic Ansible Standards testing

This repository is hosting Python tests built into images for automated ansible standards testing.
These tests are updated automatically from Red Hat's internal instance of Gitlab,
built in quay and tagged with corresponding version:

- https://quay.io/repository/exd_infra_plts_cip/ansible_standards_testing
- https://quay.io/repository/exd_infra_plts_cip/ansible_standards_production
