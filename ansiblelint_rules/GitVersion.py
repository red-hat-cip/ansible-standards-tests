from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union

import os
import re


class GitVersion(AnsibleLintRule):
    id = "508"
    shortdesc = 'Git tags have to be in format x.y.z'
    description = 'Git tags have to be in format x.y.z'
    tags = ["regex", "match-task"]

    git_tag = os.environ.get("CI_COMMIT_TAG", "")
    regex = re.compile(r'^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<p'
                       r'atch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*'
                       r'[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z'
                       r'-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA'
                       r'-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$')

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        return not (self.git_tag == "" or self.regex.match(self.git_tag))

#Build Thu May 27 20:52:24 UTC 2021
