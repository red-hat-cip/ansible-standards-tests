from ansiblelint.rules import AnsibleLintRule
from typing import Union

import os


class PlaybookExtension(AnsibleLintRule):
    id = '107'
    shortdesc = 'Playbooks should have the ".yml" and not ".yaml" extension'
    description = 'Playbooks should have the ".yml" and not ".yaml" extension'
    tags = ['playbook', 'extension', 'match-line']

    done = []  # already noticed path list

    def match(self, file, text: str) -> Union[bool, str]:
        if file['type'] != 'playbook':
            return False

        path = file['path']
        ext = os.path.splitext(path)
        if ext[1] != ".yml" and path not in self.done:
            self.done.append(path)
            return True
        return False

#Build Thu May 27 20:52:24 UTC 2021
