from ansiblelint.rules import AnsibleLintRule
from typing import Union, Any, Dict

import os
import re


class ModulePrefix(AnsibleLintRule):
    id = "507"
    shortdesc = (
        'All defaults to roles modules should be prefixed with {rolename}_'
    )
    tags = ['module', 'formatting', 'match-task', 'regex']

    def_checked = False
    regex = re.compile(r"^[\w][^#-][\w]*:")
    rolename = os.environ.get("CI_PROJECT_NAME", "")

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if self.rolename != "" or not self.def_checked:
            if(os.path.isfile(os.getcwd() + "/defaults/main.yml")):
                mod_def_file = open(os.getcwd() + "/defaults/main.yml")
                mod_file_lines = mod_def_file.readlines()
            else:
                return False
            for line in mod_file_lines:
                tmp_line = self.regex.match(line)
                if tmp_line is not None and \
                   not tmp_line.group(0).startswith(self.rolename + "_"):
                    return True
            self.def_checked = True
        return False

#Build Thu May 27 20:52:24 UTC 2021
