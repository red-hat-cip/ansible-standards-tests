from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union
from ansiblelint.utils import get_first_cmd_arg

import os


try:
    from ansible.module_utils.parsing.convert_bool import boolean
except ImportError:
    try:
        from ansible.utils.boolean import boolean
    except ImportError:
        try:
            from ansible.utils import boolean
        except ImportError:
            from ansible import constants
            boolean = constants.mk_boolean


class CommandsInsteadOfModulesRule(AnsibleLintRule):
    id = '303'
    shortdesc = 'Using command rather than module'
    description = (
        'Executing a command when there is an Ansible module '
        'is generally a bad idea'
    )
    tags = ['command', 'shell', 'resources', 'module', 'match-task']

    _commands = ['command', 'shell']
    _modules = {
        'apt-get': 'apt-get',
        'chkconfig': 'service',
        'curl': 'get_url or uri',
        'git': 'git',
        'hg': 'hg',
        'letsencrypt': 'acme_certificate',
        'mktemp': 'tempfile',
        'mount': 'mount',
        'patch': 'patch',
        'rpm': 'yum or rpm_key',
        'rsync': 'synchronize',
        'sed': 'template, replace or lineinfile',
        'service': 'service',
        'supervisorctl': 'supervisorctl',
        'svn': 'subversion',
        'systemctl': 'systemd',
        'tar': 'unarchive',
        'unzip': 'unarchive',
        'wget': 'get_url or uri',
        'yum': 'package',
        'dnf': 'package',
        'upstart': 'service',
        'systemd': 'service'
    }

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if task['action']['__ansible_module__'] not in self._commands:
            return False

        first_cmd_arg = get_first_cmd_arg(task)
        if not first_cmd_arg:
            return False

        executable = os.path.basename(first_cmd_arg)
        if executable in self._modules and \
                boolean(task['action'].get('warn', True)):
            message = '{0} used in place of {1} module'
            return message.format(executable, self._modules[executable])

#Build Thu May 27 20:52:24 UTC 2021
