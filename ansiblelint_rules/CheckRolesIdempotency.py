from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union


class CheckRolesIdempotency(AnsibleLintRule):
    id = '309'
    shortdesc = 'Commands should not change things if nothing needs doing'
    description = (
        'The role should work in check mode, meaning that first of all, they'
        'should not fail check mode, and they should also not report changes'
        'when there are no changes to be done.'

        'Idempotency-Roles should not perform changes when applied a second'
        'time to the same system with the same parameters, and it should not'
        'report that changes have been done if they have not been done.'
    )
    tags = ['command-shell', 'idempotency', 'match-task']

    _commands = ['command', 'shell', 'raw']

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if task["__ansible_action_type__"] == 'task':
            if task["action"]["__ansible_module__"] in self._commands:
                return 'changed_when' not in task and \
                    'when' not in task and \
                    'creates' not in task['action'] and \
                    'removes' not in task['action']

#Build Thu May 27 20:52:24 UTC 2021
