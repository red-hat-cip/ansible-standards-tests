from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union


class DontUseLineinfileRule(AnsibleLintRule):
    id = '102'
    shortdesc = 'The lineinfile module is typically nasty'
    description = (
        'While lineinfile supports some idemptotency, using template or '
        'assemble modules to populate configuration files is preferred'
    )
    tags = ['module', 'lineinfile', 'match-task']

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        return task["action"]["__ansible_module__"] == 'lineinfile'

#Build Thu May 27 20:52:24 UTC 2021
