from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union


class MetaEndplayRule(AnsibleLintRule):
    id = '405'
    shortdesc = 'meta: end_play is not permitted'
    description = (
        'Do not use meta: end_play. Using meta: end_host is preferred.'
    )
    tags = ['meta', 'match-task']

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if task["__ansible_action_type__"] != 'task':
            return False

        action = task.get('action')

        if action.get("__ansible_module__") != 'meta':
            return False

        return 'end_play' in action.get("__ansible_arguments__")

#Build Thu May 27 20:52:24 UTC 2021
