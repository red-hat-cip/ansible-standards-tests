from ansiblelint.rules import AnsibleLintRule
from typing import Union

import re


class DontUseTrueFalseBoolean(AnsibleLintRule):
    id = '606'
    shortdesc = 'Use true/false instead of True/False'
    description = 'Use true/false instead of True/False'
    tags = ['formatting', 'match-line', 'regex']

    compiled = re.compile(r'\:\s(True|False)$')

    def match(self, text: str) -> Union[bool, str]:
        m = self.compiled.search(text)
        if m:
            return True
        return False

#Build Thu May 27 20:52:24 UTC 2021
