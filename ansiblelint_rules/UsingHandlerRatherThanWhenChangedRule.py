from ansiblelint.rules import AnsibleLintRule
from typing import Union, Any, Dict


def _changed_in_when(item):
    if not isinstance(item, str):
        return False
    return any(changed in item for changed in
               ['.changed', '|changed', '["changed"]', "['changed']"])


class UsingHandlerRatherThanWhenChangedRule(AnsibleLintRule):
    id = '503'
    shortdesc = 'Tasks that run when changed should likely be handlers'
    description = (
        'If a task has a ``when: result.changed`` setting, it is effectively '
        'acting as a handler. Avoid use of when: foo_result'
    )
    tags = ['behaviour', 'formatting', 'match-task']

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if task["__ansible_action_type__"] != 'task':
            return False

        when = task.get('when')

        if isinstance(when, list):
            for item in when:
                return _changed_in_when(item)
        else:
            return _changed_in_when(when)

#Build Thu May 27 20:52:24 UTC 2021
