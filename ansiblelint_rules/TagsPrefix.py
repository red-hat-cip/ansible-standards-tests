from ansiblelint.rules import AnsibleLintRule
from typing import Union, Any, Dict

import os
import re


class TagsPrefix(AnsibleLintRule):
    id = "506"
    shortdesc = 'Tags should be prefixed with {rolename}_'
    description = 'Tags should be prefixed with {rolename}_'
    tags = ['tags', 'formatting', 'roles', 'regex', 'match-task']

    role_name = os.environ.get("CI_PROJECT_NAME", "")
    tag_regex = ""

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if(self.role_name != ""):
            self.tag_regex = re.compile(self.role_name + "_.*$")
        if self.role_name != "" and 'tags' in task and self.tag_regex != "":
            if isinstance(task['tags'], str):
                if not self.tag_regex.match(task['tags']):
                    return True
            else:
                for tag in task['tags']:
                    if not self.tag_regex.match(tag):
                        return True
        return False

#Build Thu May 27 20:52:24 UTC 2021
