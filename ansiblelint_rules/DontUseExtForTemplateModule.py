from ansiblelint.rules import AnsibleLintRule
from typing import Any, Dict, Union

import os


class DontUseExtForTemplateModule(AnsibleLintRule):
    id = '402'
    shortdesc = 'Template files should not have the extension \'.j2\''
    description = (
        'When using the template module, refrain from appending .j2 to the '
        'file name'
    )
    tags = ['module', 'template', 'extension', 'jinja', 'match-task']

    def matchtask(self, task: Dict[str, Any]) -> Union[bool, str]:
        if task['action']['__ansible_module__'] != 'template':
            return False
        ext = os.path.splitext(task['action']['src'])
        return ext[1] == ".j2"

#Build Thu May 27 20:52:24 UTC 2021
