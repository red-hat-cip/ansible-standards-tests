from ansiblelint.rules import AnsibleLintRule
from ansiblelint.file_utils import Lintable
from ansiblelint.errors import MatchError

from typing import List
from os import path
from ipaddress import IPv6Address
import re


class AddressesExamples(AnsibleLintRule):
    id = '211'
    shortdesc = 'Use RFC 5737, 7042, 3849 addresses in examples'
    description = 'Use RFC 5737, 7042, 3849 addresses in examples'
    tags = ['formatting', 'regex', 'match-line']

    # List of IPv4 address scopes regexes assigned for documentation
    ipv4_regex_list = (
        r'192\.0\.2\.[0-9]{1,3}/24',
        r'192\.0\.2\.[0-9]{1,3}',
        r'198\.51\.100\.[0-9]{1,3}/24',
        r'198\.51\.100\.[0-9]{1,3}',
        r'203\.0\.113\.[0-9]{1,3}/24',
        r'203\.0\.113\.[0-9]{1,3}'
    )

    # Lower and upper bounds of IPv6 documentation scope
    ipv6_lowbound = 42540766411282592856903984951653826560
    ipv6_higbound = 42540766490510755371168322545197776895

    # Regexes for EUI48 addresses assigned for documentation
    eui48_regex_list = (
        r'00-00-5E-00-53-[A-F0-9]{2}',
        r'00:00:5E:00:53:[A-F0-9]{2}',
        r'01-00-5E-90-10-[A-F0-9]{2}',
        r'01:00:5E:90:10:[A-F0-9]{2}'
    )

    # Regexes for EUI64 addresses assigned for documentation
    eui64_regex_list = (
        r'0(?:0|1)-00-5E-EF-10-00-00-[A-F0-9]{2}',
        r'0(?:0|1):00:5E:EF:10:00:00:[A-F0-9]{2}',
        r'0(?:0|1)-00-5E-FE-C0-00-02-[A-F0-9]{2}',
        r'0(?:0|1):00:5E:FE:C0:00:02:[A-F0-9]{2}',
        r'0(?:0|1)-00-5E-FE-C6-33-64-[A-F0-9]{2}',
        r'0(?:0|1):00:5E:FE:C6:33:64:[A-F0-9]{2}',
        r'0(?:0|1)-00-5E-FE-CB-00-71-[A-F0-9]{2}',
        r'0(?:0|1):00:5E:FE:CB:00:71:[A-F0-9]{2}',
        r'0(?:0|1)-00-5E-FE-EA-C0-00-02',
        r'0(?:0|1):00:5E:FE:EA:C0:00:02',
        r'0(?:0|1)-00-5E-FE-EA-C6-33-64',
        r'0(?:0|1):00:5E:FE:EA:C6:33:64',
        r'0(?:0|1)-00-5E-FE-EA-CB-00-71',
        r'0(?:0|1):00:5E:FE:EA:CB:00:71',
        r'00-00-5E-FF-FE-00-53-[A-F0-9]{2}',
        r'00:00:5E:FF:FE:00:53:[A-F0-9]{2}',
        r'01-00-5E-FF-FE-90-10-[A-F0-9]{2}',
        r'01:00:5E:FF:FE:90:10:[A-F0-9]{2}'
    )

    # Compiled lists of documentation assigned IPv4|EUI48/64 addresses regexes
    ipv4_regex_list_compiled = \
        [re.compile(regex, re.IGNORECASE) for regex in ipv4_regex_list]
    eui48_regex_list_compiled = \
        [re.compile(regex, re.IGNORECASE) for regex in eui48_regex_list]

    eui64_regex_list_compiled = \
        [re.compile(regex, re.IGNORECASE) for regex in eui64_regex_list]

    # Compiled regexes for finding every IP and EUI
    ipv4_regex = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d\d?)?')
    ipv6_regex = re.compile(r'(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-'
                            r'9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}'
                            r':[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0'
                            r'-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}('
                            r':[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,'
                            r'3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:)'
                            r'{1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}'
                            r':((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,'
                            r'4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9'
                            r'a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0'
                            r'-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25'
                            r'[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-f'
                            r'A-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]'
                            r'){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-'
                            r'9]){0,1}[0-9]))')
    eui48_regex = re.compile(r'(?!:|-)'
                             r'(?:(?:[A-F0-9]{2}(?::[A-F0-9]{2}){5})|'
                             r'(?:[A-F0-9]{2}(?:-[A-F0-9]{2}){5}))'
                             r'(?!:|-)',
                             re.IGNORECASE)
    eui64_regex = re.compile(r'(?!:|-)'
                             r'(?:[A-F0-9]{2}(?::[A-F0-9]{2}){7})|'
                             r'(?:[A-F0-9]{2}(?:-[A-F0-9]{2}){7})'
                             r'(?!:|-)',
                             re.IGNORECASE)

    def matchlines(self, file: "Lintable") -> List[MatchError]:

        # Empty list of founded errors
        matches: List[MatchError] = []

        # Target only default values
        if 'defaults' in path.dirname(file.path):
            for (prev_line_no, line) in enumerate(file.content.split("\n")):
                # Load every address on line
                ipv4 = self.ipv4_regex.findall(line)
                ipv6 = self.ipv6_regex.findall(line)
                eui64 = self.eui64_regex.findall(line)
                # eui48 regex is catching even eui64 addresses
                if not eui64:
                    eui48 = self.eui48_regex.findall(line)
                else:
                    eui48 = None

                if ipv4:
                    found_toggle = False
                    for regex in self.ipv4_regex_list_compiled:
                        if regex.match(ipv4[0]):
                            found_toggle = True
                    if not found_toggle:
                        m = self.create_matcherror(
                            message=self.shortdesc,
                            linenumber=prev_line_no + 1,
                            details=line,
                            filename=file
                        )
                        matches.append(m)

                if ipv6:
                    decimal = int(IPv6Address(ipv6[0][0]))
                    if not self.ipv6_lowbound <= decimal <= self.ipv6_higbound:
                        m = self.create_matcherror(
                            message=self.shortdesc,
                            linenumber=prev_line_no + 1,
                            details=line,
                            filename=file,
                        )
                        matches.append(m)

                if eui48:
                    found_toggle = False
                    for regex in self.eui48_regex_list_compiled:
                        if regex.match(eui48[0]):
                            found_toggle = True
                    if not found_toggle:
                        m = self.create_matcherror(
                            message=self.shortdesc,
                            linenumber=prev_line_no + 1,
                            details=line,
                            filename=file
                        )
                        matches.append(m)

                if eui64:
                    found_toggle = False
                    for regex in self.eui64_regex_list_compiled:
                        if regex.match(eui64[0]):
                            found_toggle = True
                    if not found_toggle:
                        m = self.create_matcherror(
                            message=self.shortdesc,
                            linenumber=prev_line_no + 1,
                            details=line,
                            filename=file
                        )
                        matches.append(m)
        return matches

#Build Thu May 27 20:52:24 UTC 2021
