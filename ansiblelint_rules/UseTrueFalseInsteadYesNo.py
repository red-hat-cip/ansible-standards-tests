from ansiblelint.rules import AnsibleLintRule
from typing import Union

import re


class UseTrueFalseInsteadYesNo(AnsibleLintRule):
    id = '603'
    shortdesc = 'Use true/false instead of yes/no'
    description = 'Use true/false instead of yes/no'
    tags = ['formatting', 'regex', 'match-line']

    compiled = re.compile(r'\:\s(yes|no)\s', re.IGNORECASE)

    def match(self, text: str) -> Union[bool, str]:
        m = self.compiled.search(text)
        if m:
            return True
        return False

#Build Thu May 27 20:52:24 UTC 2021
