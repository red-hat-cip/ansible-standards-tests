from ansiblelint.rules import AnsibleLintRule
from typing import Union

import re


class VariableHasSpacesRule(AnsibleLintRule):
    id = '206'
    shortdesc = 'Variables should have spaces before and after: {{ var_name }}'
    description = (
        'Variables should have spaces before and after: ``{{ var_name }}``'
    )
    tags = ['formatting', 'regex', 'jinja', 'match-line']

    variable_syntax = re.compile(r"{{.*}}")
    bracket_regex = re.compile(r"{{[^{' -]|[^ '}-]}}")

    def match(self, text: str) -> Union[bool, str]:
        if not self.variable_syntax.search(text):
            return
        line_exclude_json = re.sub(r"[^{]{'\w+': ?[^{]{.*?}}", "", text)
        return self.bracket_regex.search(line_exclude_json)

#Build Thu May 27 20:52:24 UTC 2021
